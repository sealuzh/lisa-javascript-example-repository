/* modified examples originally taken from
 * http://betterexplained.com/articles/the-single-page-javascript-overview/?PageSpeed=noscript
 */

function Types() {
  typeof("string") == "string";
  typeof(3) == typeof(3.4) == typeof(0x34) == "number";
  typeof(myObject) == typeof(myArray) == "object";
  typeof(true) == typeof(1 == 2) == "boolean";
  typeof(Math.sin) == "function";
  typeof(notthere) == "undefined";

  123 == "123";
  123 === "123";
  typeof(x) == "undefined";
  x = {x: null};
  x.x == null;
  x.y == undefined;

  parseInt("123");
  parseInt("123", 16);
  parseFloat("123.43");

  isNaN(0/0) == true;
  3/0 == Infinity;
  -3/0 == -Infinity;
  isFinite(3/0) == false;
}

// MCC: 3, UniquePaths: 4
function Conditions() {
  matches = "hello".match(/h../);

  re = new RegExp("h..", "ig");
  matches = "hello".match(re);

  "hello".replace(/h/,"b");

  var str = "Yellow";
  if (str == "Hello"){
    console.log("Hi");
  }
  else{
    console.log("something is wrong!");
  }

  a = 3, b = 4;
  c = a > b ? a : b;

  // Taken from http://stackoverflow.com/questions/7202157/why-does-return-the-string-10
  ++[[]][+[]]+[+[]] == "10";
}

// MCC: 7, UniquePaths: 4
function ControlFlow() {
  var name = "Joe";
  // MCC: 4, UniquePaths: 24
  switch (name){
    case "Bob":
      console.log("Hi Bob!")
      break
    case "Joe":
      console.log("Hey Joe.")
      break
    default: console.log("Do I know you?")
  }

  var n = 4;
  var i = 0;
  while (i < n){
   i++;
  }
  console.log(n);

  for (var i=0; i<n; i++){
    console.log(i);
  }

  obj = {x: 1, y: 2, n: 10};
  for (var key in obj){
    console.log(key + ":" + obj[key]);
  }
}

function Functions() {
  function foo(a,b){
    return a + b;
  }

  var fn = function(a,b){
    return foo(a,b);
  }

  obj.fn = function(a,b){
    return a + b;
  }

  function bar(a,b){
      var n = 1;

      function helper(x) {
          return 1/Math.sqrt(x + n);
      }
      return helper(input);
  }

  foo(1,2) == fn(1,2) == 3;
}

function Constructors() {
  function Person(first, last) {
      this.first = first;
      this.last = last;

      var privateFn = function(first, last){

      }

      this.setName = function(first, last){
          this.first = first;
      this.last = last;
      }

  }

  Person.prototype.fullName = function() {
      return this.first + ' ' + this.last;
  }

  var bob = new Person("Bob", "Smith");
  bob.fullName();
}

function Exceptions() {
  function foo(a,b){
    var c = a + b;
    throw "Error Message";
  }

  try{
     foo(1,2);
  }
  catch(e){
    console.log(e);
  }
}

function Misc() {
  eval("x = 3");

  timer = setTimeout("myfunction()", 1000)
  clearTimeout(timer);

  x = document.getElementById("test");

  x.style.width = "50px";
  x.style.color = "#ffffff";
  x.style.background = "#333";
  x.style.borderLeft = "1px solid #ccc";

  x.className = "myclass";
  x.innerHTML = "Hello";

  y = document.getElementById("testInput");
  y.value = "Hi";

}

